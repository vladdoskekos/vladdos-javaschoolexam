package com.tsystems.javaschool.tasks.calculator;

public class Calculator {
    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String expression) {
        if(expression == null)
            return null;
        if(!isValid(expression)){
            return null;
        }
        expression = convertToNormal(expression);
        Double x = solveOperation(expression);
        if(Double.isInfinite(x))                      //check if there is no zero division
            return null;
        if( x % 1 == 0 )
            return String.valueOf((int)Math.round(x));
        return String.valueOf(round(x, 4));
    }
    //check if expression is valid
    private boolean isValid(String expression){

        if(expression.equals("")){
            return false;
        }

        String[] invalid_mas = new String[]{"++", "--", "-+", "..", "-*", "-/", "**", "//"
                                            , "/*", "*/", "/)", "(/", "(*", "*)", " "};
        for(String i:invalid_mas){
            if(expression.contains(i))
                return false;
        }

        for(int i = 0; i < expression.length(); i++){
            char t = expression.charAt(i);
            if(Character.isDigit(t) || t == '+' || t == '-' || t == '.' || t == '/' || t == '*' || t == '(' || t == ')'){ }
            else
                return false;
        }

        if(expression.contains("(")) {
            for (int i = 0; i < expression.length(); i++) {
                if (expression.charAt(i) == '(')
                    if (getIndexAppropriateBracket(i, expression) == -3)
                        return false;
            }
        }

        if(expression.contains(")") && !expression.contains("("))
            return false;

        return true;
    }

    //delete spaces, convert "+-" into "-" and replace "/-" "*-" with temporary symbols which will be used instead of them
    // $ = /
    // # = *
    private String convertToNormal(String expression){
        expression = expression.replaceAll("\\s", "");
        if(expression.contains("+-"))
            expression = expression.replace("+-", "-");
        if(expression.contains("--"))
            expression = expression.replace("--", "+");
        if(expression.contains("/-"))
            expression = expression.replace("/-", "$");
        if(expression.contains("*-"))
            expression = expression.replace("*-", "#");
        return expression;
    }

    //get index of bracket which corresponds to '('
    private int getIndexAppropriateBracket(int bracket_index, String expression){
        int priority = 0;
        for(int i = bracket_index + 1; i < expression.length(); i++){
            if(expression.charAt(i) == ')'){
                priority--;
                if(priority == -1){
                    return i;
                }
            }
            if(expression.charAt(i) == '('){
                priority++;
            }
        }
        return -3;
    }


    //Find sum and difference
    private double solveOperation(String expression) {
        double t = 0;
        String temp = "0";
        boolean sum_residual = true;
        char l;
        for(int i = 0; i < expression.length(); i++){
            l = expression.charAt(i);
            if(Character.isDigit(l) || l == '*' || l == '/' || l == '$' || l == '#' || l == '.'){
                temp += expression.charAt(i);
            }
            else if(expression.charAt(i) == '+'){
                temp = checkMultDiv(temp);
                if(temp.contains("(")){
                    if(sum_residual == true)
                        t += solveOperation(temp.substring(temp.indexOf('(')+1, getIndexAppropriateBracket(temp.indexOf('('), temp)));
                    else
                        t -= solveOperation(temp.substring(temp.indexOf('(')+1, getIndexAppropriateBracket(temp.indexOf('('), temp)));
                    temp = "";
                    sum_residual = true;
                    continue;
                }
                if(sum_residual == true)
                    t += Double.parseDouble(temp);
                else
                    t -= Double.parseDouble(temp);
                sum_residual = true;
                temp = "";
            }
            else if(expression.charAt(i) == '-'){
                temp = checkMultDiv(temp);
                if(temp.contains("(")){
                    if(sum_residual == true)
                        t += solveOperation(temp.substring(temp.indexOf('(')+1, getIndexAppropriateBracket(temp.indexOf('('), temp)));
                    else
                        t -= solveOperation(temp.substring(temp.indexOf('(')+1, getIndexAppropriateBracket(temp.indexOf('('), temp)));
                    temp = "";
                    sum_residual = false;
                    continue;
                }
                temp = checkMultDiv(temp);
                if(sum_residual == true)
                    t += Double.parseDouble(temp);
                else
                    t -= Double.parseDouble(temp);
                sum_residual = false;
                temp = "";
            }
            else if(expression.charAt(i) == ('(')){

                temp += expression.substring(i, getIndexAppropriateBracket(i, expression)+1);

                if(i != expression.length()-1)
                    i = getIndexAppropriateBracket(i, expression) + 1;
                else
                    i = getIndexAppropriateBracket(i, expression);
                i--;
            }
            if(i == expression.length()-1){
                temp = checkMultDiv(temp);
                if(temp.contains("(")) {
                    if (sum_residual == true)
                        t += solveOperation(temp.substring(temp.indexOf('(') + 1, getIndexAppropriateBracket(temp.indexOf('('), temp)));
                    else
                        t -= solveOperation(temp.substring(temp.indexOf('(') + 1, getIndexAppropriateBracket(temp.indexOf('('), temp)));
                    temp = "";
                    continue;
                }

                if(sum_residual == true)
                    t += Double.parseDouble(temp);
                if(sum_residual == false)
                    t -= Double.parseDouble(temp);
            }
        }
        return t;
    }


    //Check if there are mult, division in the expression
    private String checkMultDiv(String expression){

        String[] check_mas = new String[]{"*", "/", "$", "#"};
        for(int i = 0; i < check_mas.length; i++){
            if(expression.contains(check_mas[i])){

                return String.valueOf(getMultDiv(expression));
            }
        }
        return expression;
    }

    private double determAction(double t, int action, String expression){
        switch (action){
            case 0:
                t *= Double.parseDouble(expression);
                break;
            case 1:
                t /= Double.parseDouble(expression);
                break;
            case 2:
                t /= -Double.parseDouble(expression);
                break;
            case 3:
                t *= -Double.parseDouble(expression);
                break;
        }
        return t;
    }

    //Multiplication and division
    private double getMultDiv(String expression){
        double t = 1;
        String temp = "";
        int check_operation = 0;
        for(int i = 0; i < expression.length(); i++){
            if(Character.isDigit(expression.charAt(i)) || (expression.charAt(i) == '-' && i == 0) || expression.charAt(i) == '.'){
                temp += expression.charAt(i);
            }
            else if(expression.charAt(i) == '*'){
                if(temp.contains("(") ){
                    if(check_operation == 0)
                        t *= solveOperation(temp.substring(temp.indexOf('(')+1, getIndexAppropriateBracket(temp.indexOf('('), temp)));
                    else if(check_operation == 1)
                        t /= solveOperation(temp.substring(temp.indexOf('(')+1, getIndexAppropriateBracket(temp.indexOf('('), temp)));
                    else if(check_operation == 2)
                        t /= -solveOperation(temp.substring(temp.indexOf('(')+1, getIndexAppropriateBracket(temp.indexOf('('), temp)));
                    else if(check_operation ==3)
                        t *= -solveOperation(temp.substring(temp.indexOf('(')+1, getIndexAppropriateBracket(temp.indexOf('('), temp)));
                    temp = "";
                    check_operation = 0;
                    continue;
                }
                t = determAction(t,check_operation, temp);
                check_operation = 0;
                temp = "";
            }
            else if(expression.charAt(i) == '/'){
                if(temp.contains("(") ){
                    if(check_operation == 0)
                        t *= solveOperation(temp.substring(temp.indexOf('(')+1, getIndexAppropriateBracket(temp.indexOf('('), temp)));
                    else if(check_operation == 1)
                        t /= solveOperation(temp.substring(temp.indexOf('(')+1, getIndexAppropriateBracket(temp.indexOf('('), temp)));
                    else if(check_operation == 2)
                        t /= -solveOperation(temp.substring(temp.indexOf('(')+1, getIndexAppropriateBracket(temp.indexOf('('), temp)));
                    else if(check_operation ==3)
                        t *= -solveOperation(temp.substring(temp.indexOf('(')+1, getIndexAppropriateBracket(temp.indexOf('('), temp)));
                    temp = "";
                    check_operation = 1;
                    continue;
                }
                t = determAction(t,check_operation, temp);
                check_operation = 1;
                temp = "";
            }
            else if(expression.charAt(i) == '$'){
                if(temp.contains("(") ){
                    if(check_operation == 0)
                        t *= solveOperation(temp.substring(temp.indexOf('(')+1, getIndexAppropriateBracket(temp.indexOf('('), temp)));
                    else if(check_operation == 1)
                        t /= solveOperation(temp.substring(temp.indexOf('(')+1, getIndexAppropriateBracket(temp.indexOf('('), temp)));
                    else if(check_operation == 2)
                        t /= -solveOperation(temp.substring(temp.indexOf('(')+1, getIndexAppropriateBracket(temp.indexOf('('), temp)));
                    else if(check_operation ==3)
                        t *= -solveOperation(temp.substring(temp.indexOf('(')+1, getIndexAppropriateBracket(temp.indexOf('('), temp)));
                    temp = "";
                    check_operation = 2;
                    continue;
                }
                t = determAction(t,check_operation, temp);
                check_operation = 2;
                temp = "";
            }
            else if(expression.charAt(i) == '#'){
                if(temp.contains("(") ){
                    if(check_operation == 0)
                        t *= solveOperation(temp.substring(temp.indexOf('(')+1, getIndexAppropriateBracket(temp.indexOf('('), temp)));
                    else if(check_operation == 1)
                        t /= solveOperation(temp.substring(temp.indexOf('(')+1, getIndexAppropriateBracket(temp.indexOf('('), temp)));
                    else if(check_operation == 2)
                        t /= -solveOperation(temp.substring(temp.indexOf('(')+1, getIndexAppropriateBracket(temp.indexOf('('), temp)));
                    else if(check_operation ==3)
                        t *= -solveOperation(temp.substring(temp.indexOf('(')+1, getIndexAppropriateBracket(temp.indexOf('('), temp)));
                    temp = "";
                    check_operation = 3;
                    continue;
                }
                t = determAction(t,check_operation, temp);
                check_operation = 3;
                temp = "";
            }
            else if(expression.charAt(i) == ('(')){
                temp += expression.substring(i, getIndexAppropriateBracket(i, expression)+1);
                if(i != expression.length()-1)
                    i = getIndexAppropriateBracket(i, expression) + 1;
                else
                    i = getIndexAppropriateBracket(i, expression);
                i--;
            }
            if(i == expression.length()-1){
                if(temp.contains("(") ){
                    if(check_operation == 0)
                        t *= solveOperation(temp.substring(temp.indexOf('(')+1, getIndexAppropriateBracket(temp.indexOf('('), temp)));
                    else if(check_operation == 1)
                        t /= solveOperation(temp.substring(temp.indexOf('(')+1, getIndexAppropriateBracket(temp.indexOf('('), temp)));
                    else if(check_operation == 2)
                        t /= -solveOperation(temp.substring(temp.indexOf('(')+1, getIndexAppropriateBracket(temp.indexOf('('), temp)));
                    else if(check_operation ==3)
                        t *= -solveOperation(temp.substring(temp.indexOf('(')+1, getIndexAppropriateBracket(temp.indexOf('('), temp)));
                    temp = "";
                    check_operation = 0;
                    continue;
                }
                t = determAction(t,check_operation, temp);
            }
        }
        return t;
    }

    public double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();
        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }

}
